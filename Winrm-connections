<#
.SYNOPSIS
This script enables PowerShell remoting and configures the local Windows firewall to allow WinRM access.

.DESCRIPTION
This script is intended to be executed on a Windows host to enable PowerShell remoting and configure the local Windows firewall to allow WinRM access. It performs the following tasks:
1. Sets the WinRM service to start automatically.
2. Starts the WinRM service.
3. Configures the WinRM listener to accept remote requests.
4. Configures the local Windows firewall to allow WinRM traffic.

.NOTES
- This script requires administrative privileges to run.
- This script modifies system settings and should be used with caution.

.EXAMPLE
.\ConfigureRemotingForAnsible.ps1
Runs the script to enable PowerShell remoting and configure the Windows firewall for WinRM access.

#>

# Enable PowerShell remoting
Write-Host "Enabling PowerShell remoting..."
Enable-PSRemoting -Force | Out-Null

# Start the WinRM service
Write-Host "Starting the WinRM service..."
Start-Service WinRM

# Set the WinRM service to start automatically
Write-Host "Setting the WinRM service to start automatically..."
Set-Service WinRM -StartupType Automatic

# Configure the WinRM listener to accept remote requests
Write-Host "Configuring the WinRM listener to accept remote requests..."
winrm create winrm/config/Listener?Address=*+Transport=HTTP | Out-Null
winrm create winrm/config/Listener?Address=*+Transport=HTTPS | Out-Null

# Configure the local Windows firewall to allow WinRM traffic
Write-Host "Configuring the local Windows firewall to allow WinRM traffic..."
netsh advfirewall firewall add rule name="WinRM HTTP" dir=in localport=5985 protocol=TCP action=allow | Out-Null
netsh advfirewall firewall add rule name="WinRM HTTPS" dir=in localport=5986 protocol=TCP action=allow | Out-Null

Write-Host "PowerShell remoting has been enabled, and the Windows firewall has been configured to allow WinRM traffic."
